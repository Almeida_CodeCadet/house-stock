package org.lasan.housestock.services;

import org.lasan.housestock.persistence.dao.ProductDao;
import org.lasan.housestock.persistence.dao.jpa.JpaProductDao;
import org.lasan.housestock.persistence.model.product.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@org.springframework.stereotype.Service
public class ProductServiceImpl implements ProductService {

    private ProductDao productDao;

    @Autowired
    public void setProductDao(ProductDao productDao) {
        this.productDao = productDao;
    }

    @Override
    public Product get(Integer id) {
        return productDao.findById(id);
    }

    @Transactional
    @Override
    public Product save(Product obj) {
        return productDao.saveOrUpdate(obj);
    }

    @Transactional
    @Override
    public void delete(Integer id) {
        productDao.delete(id);
    }

    @Override
    public List<Product> list() {
        return productDao.listItems();
    }

    @Override
    public List<Product> expiringProducts() {
        return null;
    }

    @Override
    public List<Product> shoppingList() {
        return null;
    }
}
