package org.lasan.housestock.services;

import org.lasan.housestock.persistence.model.product.Product;

import java.util.List;

public interface ProductService {

    /**
     * Gets the product with the given id
     *
     * @param id the product id
     * @return the product
     */
    Product get(Integer id);


    /**
     * Saves a product
     *
     * @return product the saved product
     */
    Product save(Product product);


    /**
     * Deletes the product
     *
     * @param id the products id
     */
    void delete(Integer id);


    /**
     * Gets a list of the products
     *
     * @return the products list
     */
    List<Product> list();


    /**
     * Gets a list of the almost expiring products
     *
     * @return the products list
     */
    List<Product> expiringProducts();


    /**
     * Generates a list of the products that are bellow minimum stock level
     *
     * @return the products list
     */
    List<Product> shoppingList();

}