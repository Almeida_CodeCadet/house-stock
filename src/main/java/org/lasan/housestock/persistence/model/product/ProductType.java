package org.lasan.housestock.persistence.model.product;

public enum ProductType {
    Protein,        // meat, fish, veggie
    Charcuterie,    // ham, cheese
    Canned,         // grain, beans
    Powder,         // sugar, salt, spices
    Cereals,        //
    Dairy,          // milk, cheese
    Cakes,          // cake ingredients ?
    Drinks,
    Fresh;          // fresh veggies, fruits

}
