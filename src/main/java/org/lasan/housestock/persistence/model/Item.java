package org.lasan.housestock.persistence.model;

public interface Item {

    Integer getId();

    void setId(Integer id);
}
