package org.lasan.housestock.persistence.model.product;

import org.lasan.housestock.persistence.model.AbstractItem;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.util.Date;

// Groceries products: Milk, cereals, fruit, ...

@Entity //name to queries
@Table(name= "product") //database table name
public class Product extends AbstractItem {

    private String name;
    private String description;
    private Double price;
    private Date expiryDate;
    private String brand;
    private Integer quantity;  // free field as it can be: in weight (g/kg); units; liters ...
    private Integer minStockLevel; //minimum stock level

    @Enumerated(EnumType.STRING)
    private ProductType type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public ProductType getType() {
        return type;
    }

    public void setType(ProductType type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
