package org.lasan.housestock.persistence.model.user;

public enum UserRoles {

    admin,
    user;

}
