package org.lasan.housestock.persistence.dao.jpa;

import org.lasan.housestock.persistence.dao.ProductDao;
import org.lasan.housestock.persistence.model.product.Product;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class JpaProductDao extends GenericJpaDao <Product> implements ProductDao {

    public JpaProductDao() {
        super(Product.class);
    }

    @Override
    public List<Product> shoppingList() {
        return null;
    }

    @Override
    public List<Product> expiringProduct() {
        return null;
    }
}
