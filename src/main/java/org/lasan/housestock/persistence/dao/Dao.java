package org.lasan.housestock.persistence.dao;

import org.lasan.housestock.persistence.model.Item;

import java.util.List;

//CRUD

public interface Dao<T extends Item> {

    List<T> listItems();

    T findById(Integer id);

    T saveOrUpdate(T itemObject);

    void delete(Integer id);
}
