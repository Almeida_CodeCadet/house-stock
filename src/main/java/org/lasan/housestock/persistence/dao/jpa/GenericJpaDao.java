package org.lasan.housestock.persistence.dao.jpa;

import org.lasan.housestock.persistence.dao.Dao;
import org.lasan.housestock.persistence.model.Item;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class GenericJpaDao<T extends Item> implements Dao<T> {

    protected Class<T> itemType;

    @PersistenceContext
    protected EntityManager em;

    // Initializes a new JPA DAO instance given a model type
    public GenericJpaDao(Class<T> itemType) {
        this.itemType = itemType;
    }

    // Set entity manager?


    public void setEm(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<T> listItems() {

        CriteriaQuery<T> criteriaQuery = em.getCriteriaBuilder().createQuery(itemType);

        Root<T> root = criteriaQuery.from(itemType);

        return em.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public T findById(Integer id) {
        return em.find(itemType, id);
    }

    @Override
    public T saveOrUpdate(T itemObject) {
        return em.merge(itemObject);
    }

    @Override
    public void delete(Integer id) {
        em.remove(em.find(itemType, id));
    }
}
