package org.lasan.housestock.persistence.dao;

import org.lasan.housestock.persistence.model.product.Product;

import java.util.List;

public interface ProductDao extends Dao <Product> {

    List<Product> shoppingList();
    List<Product> expiringProduct();
}
