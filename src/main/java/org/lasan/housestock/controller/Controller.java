package org.lasan.housestock.controller;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@org.springframework.stereotype.Controller
@RequestMapping("/")
public class Controller {


    @RequestMapping(method = RequestMethod.GET, path = "/")
    public String getIndex(Model model) {
        System.out.println("Cheguei aqui");
        return "home";
    }

}
